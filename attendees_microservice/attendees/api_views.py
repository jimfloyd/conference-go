from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from django.views.decorators.http import require_http_methods
import json
from .models import ConferenceVO
import requests



class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "import_href", "name"
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]
    encoder = {
        "conference": ConferenceVO(),
    }

# Like you did in the last section, import the require_http_methods decorator and make api_list_attendees only respond to GET and POST.
# We're not dealing with conferences anymore. We're dealing with ConferenceVO objects. Because of that, let's change the name of the parameter of the api_list_attendees function to reflect that.
@require_http_methods(["GET", "POST", "DELETE"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """

    if request.method == "GET":
        # response = []
        # attendee = Attendee.objects.all()
        # UPDATE THE ARGUMENT NAME
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        # for i in attendees:
        #     response.append(
        #         {
        #         "name": i.name,
        #         "href": i.get_api_url(),
        #         },
        #     )
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False
            )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.get(id=id)
        return JsonResponse({"deleted:": count > 0})

    else:
        content = json.loads(request.body)
    # Get the Conference object and put it in the content dict
        try:
        # THIS LINE IS ADDED
            conference_href = f'/api/conferences/{conference_vo_id}/'

            # conference = ConferenceVO.objects.get(id=conference_id)
                    # THIS LINE CHANGES TO ConferenceVO and import_href
            conference = ConferenceVO.objects.get(import_href=conference_href)

# Now, the code expects a "conference" key in the submitted JSON-encoded data. It should be the "href" value for a Conference object.
            content["conference"] = conference

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    # ModelEncoder
        # attendees = Attendee.objects.filter(conference=conference_id)
        # return JsonResponse(
        #     {"attendees": attendees},
        #     encoder=AttendeeListEncoder,
        # )

        # To create an Attendee Entity, it needs a Conference Entity. To handle that, we need to use the identifier for the Conference that is mapped in the url and passed into the function as a parameter. Let's see what that looks like.
        # Create a new HTTP request in Insomnia in the "Attendees" folder named "Create attendee". Make it a POST request pointing to http://localhost:8000/api/conferences/1/attendees/.


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.name,
    #             "href": attendee.get_api_url(),
    #     },
    # }
    # )

    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = ConferenceVO.objects.filter(id=id).delete()
        return JsonResponse({"deleted:": count})

    else:
        content = json.loads(request.body)
        try:
            if "attendee" in content:
                attendee = Attendee.objects.get(id=id)
                content["attendee"] = attendee
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference id"},
                status=400,
            )

        Attendee.objects.update(**content)

        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
