from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet




class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}
    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
            d={}
            #  # if o has the attribute get_api_url then add its return value to the dictionary with the key "href"
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
        #  * for each name in the properties list
            for property in self.properties:
        # * get the value of that property from the model instance
        #           given just the property name
                value = getattr(o, property)
        #  added "location" to the ConferenceDetailEncoder properties and had to use an widget and add an encoders property and create a new class LocationListEncoder. We just need to update the ModelEncoder to handle this.
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
        #  * put it into the dictionary with that property name as
        #           the key
                d[property] = value
# You can now use this ModelEncoder to create encoders for each of the different function views.
                d.update(self.get_extra_data(o))
        #  * return the dictionary
            return d
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            return super().default(o)

# Add a method to your ModelEncoder named get_extra_data. In the ModelEncoder, have it return an empty dictionary.
    def get_extra_data(self, o):
        return {}
