import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_photo(city, state):
    url = f"https://api.pexels.com/search?query={city}&{state}/"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    content = json.loads(r.text)
    print (content["photos"][0]["urls"])
    return (content["photos"][0]["urls"])


def get_weather(city, state):

    params = {
        'q': f"{city}, {state}, US",
        'limit': 1,
        'Authorization': OPEN_WEATHER_API_KEY
    }

    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geocode_url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    specs_weather = {
        "lat": latitude,
        "lon": longitude,
        "Authorization": OPEN_WEATHER_API_KEY,
    }

    weather_url = "http://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(weather_url, params=specs_weather)
    weather_data = json.loads(weather_response.content)
    try:
        return {
            'temp': weather_data['main']['temp'],
            'description': weather_data['weather']['description'],
        }
    # except (KeyError, IndexError):
    #     return None

    # return response_dict
