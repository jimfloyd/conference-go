import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
from .models import State
import requests

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "photo_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name"
    ]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

# def save(self, *args, **kwargs):
#     photo_url = self.city_photo_url(self.city)
#     self.photo_url = photo_url
#     super().save(*args, **kwargs)

def city_photo_url(state, city):
    api_key_pexels = "glERDHuUlTZtqNO7FvPWCeuiKWBqWpyd9m8YrjwgQ9eU5f7DJ68yHiLt"
    header = {'Authorization': api_key_pexels},
    endpoint = "https://api.pexels.com/v1/search"
    parameters = {'query': f'{city}, {state}', 'per_page': 1},
    

    try:
        # request to api_key_pexels
        response = requests.get(endpoint, parameters=parameters, header=header)

        # parse the response JSON
        data = response.json()

        # get the photo_url from response variable
        if 'photos' in data['photos'] and len(data['photos']) > 0:
            return data['photos'][0]['src']['original']
        else:
            print(f"Sorry, there is no photo for this {city}")
            return None

    except requests.exceptions.RequestException as e:
        print(f"Error for api_pixels: {e}")
        return None


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        # response = []
        conferences = Conference.objects.all()
        # for conference in conferences:
            # response.append(
            #     {
            #         "name": conference.name,
            #         "href": conference.get_api_url(),
            #     }
            # )
        # return JsonResponse({"conferences": response})
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )

    # If you try sending the request from Insomnia again, you should get a Yellow Page of Sadness reporting that None was returned from the api_list_conferences function. That's true because we're not yet handling the POST to the function. Time to do that, now.
    # This looks eerily similar to what you did for the State lookup for the Location.
    else:
        content = json.loads(request.body)
        # Get the location object and put it in the content dict
        try:
            location =Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )

    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
            )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted:": count > 0})

    else:
        content = json.loads(request.body)
        # xyz = conference.objects.create(**content)
        try:
            if "location" in content:
                state = State.objects.get(id=content["location"])
                content["location"] = state

        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference id"},
                status=400,
            )

    conference = Conference.objects.filter(id=id).update(**content)
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    # Create an if block for the existing code to only run if the HTTP method is GET.
    # GET
    if request.method == "GET":
        """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
        """
        # Get all locations
        # response = []
        locations = Location.objects.all()
        # for location in locations:
        #     response.append(
        #     {
        #         "name": location.name,
        #         "href": location.get_api_url(),
        #     }
        # )
    # return JsonResponse({"locations": response})
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder,)
    # Now, add an else at the end of it. The else block will handle the POST.

    # create location
    else:
        # POST
        # content = json.loads(request.body) the content of the POST is a JSON-formatted string stored in request.body. We need to decode that using the json module. At the top of the file, import the json module
        content = json.loads(request.body)
# # Get the State object and put it in the content dict. If we look at the Location model, we can see that state is a ForeignKey, not a string like we have in the JSON. We can fix this one of two ways: Look up the state based on abbreviation in the JSON. Send an id value that specifically identifies the State. Let's go with the first one. Update your code to look up the State based on the value in the content dictionary in the "state" key. Then, assign it back to the dictionary. You may need to import State from .models.
        # state = State.objects.get(abbreviation=content["state"])
        # content["state"] = state
# What if someone sends the state abbreviation "ZZ" or "currants-and-whey"? We need to catch that error and return an appropriate error message and status code. Modify the code to handle the error by returning an error message and a status code of 400  . We choose to send back a 400, which means "Bad Request" because the request has bad content.
# Did not return "400 Bad Request" and the message, when pleaced TX with ZZ. UPDATE** commited out prior state and content values

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

# make a call to get images

# add in url from getimages to location

        # To create a new Location, you'll need to use json.loads to convert the JSON-formatted string in request.body, then use that with Location.objects.create.
        location = Location.objects.create(**content)
        # Finally, you can return the newly-created Location. All of that is part of the else.
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """

    # Here are the steps that the code needs to take:
# Convert the submitted JSON-formatted string into a dictionary.
# Convert the state abbreviation into a State, if it exists.
# Use that dictionary to update the existing Location.
# Return the updated Location object.
# Below is the code that does that. You'll see that you can copy and paste the majority of it from other places in the file.

    # if request.method == "DELETE":
    #     location = Location.objects.get(id=id)
    #     return JsonResponse(
    #         location,
    #         encoder=LocationDetailEncoder,
    #         safe=False,
    #     )

# Get location details
    if request.method == "GET":
        location= Location.objects.get(id=id)
        print("Accessing details of location")

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )
        # summary= getSummary(location.name, location.city)
        # return JsonResponse(
        #     {
        #         "location": location,
        #         "summary": summary,
        #     },
        #     encoder=LocationDetailEncoder, safe=False,
        # )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted:": count >0 })

    else:
    # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
        )

        # get_photo(content["city"], content["state"])

    # new code
    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )

    # location = Location.objects.get(id=id)
    # return JsonResponse(
    #     {
    #     "name": location.name,
    #     "city": location.city,
    #     "room_count": location.room_count,
    #     "created": location.created,
    #     "updated": location.updated,
    #     "state": location.state.abbreviation,
    # }

    # )
