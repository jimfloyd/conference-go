from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from .models import Status


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})

    if request.method == "GET":
        presentation = Presentation.objects.all()
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.get(id=conference_id)
        return JsonResponse({"deleted:" : count > 0 })

    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=id)
            content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference_id"},
                status=400
            )
        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        # Using the delete method returns is a tuple (like a list but its immutable, but its not going to change). First item deleted, is the count. second is the object that shows what its going to delete. If count is greater than 0 then something was deleted, if not nothing was deleted.
        # conference = Conference.objects.filter(id=id).delete()
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})

# Put request else/update the presentation
    else:
        # Calling JSON on the body property of the PUT request and store inside a variable called content
        content = json.loads(request.body)

        # Using dictionary to update presentation
        # Presentation.objects.filter(id=id).update(**content)
        # Get the updated presentation
        # presentation= Presentation.objects.get(id=id)

# update conference to location if trying to edit location. Use ID for this. 
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content("conference"))
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference id"},
                status=400,
            )

        Presentation.objects.update(**content)

        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


    # presentation = Presentation.objects.get(id=id)
    # return JsonResponse(
    #     presentation,
    #     encoder=PresentationDetailEncoder,
    #     safe=False,
    # )


    # presenter = Presentation.objects.get(id=id)
    # presenter_details = {
    #     "presenter_name": presenter.presenter_name,
    #     "company_name": presenter.company_name,
    #     "presenter_email": presenter.presenter_email,
    #     "title": presenter.title,
    #     "synopsis": presenter.synopsis,
    #     "created": presenter.created,
    #     "status": presenter.status,
    #     "conference": {
    #         "name": presenter.conference,
    #         "href": presenter.conference
    #     }
    # }
    # return JsonResponse({"presentation": presenter_details})

    # presentations = Presentation.objects.get(id=id)
    # return JsonResponse(
    # {
    #     "presenter_name": presentations.presenter_name,
    #     "company_name": presentations.company_name,
    #     "presenter_email": presentations.presenter_email,
    #     "title": presentations.title,
    #     "synopsis": presentations.synopsis,
    #     "created": presentations.created,

    #     # Status and conference are forgien Keys ****************
    # #     "status": presentations.status.name,
    # #     "conference": {
    # #         "name": presentations.conference.name,
    # #         "href": presentations.conference.get_api_url(),
    #     }
    # )
